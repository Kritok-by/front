import Vue from 'vue';
import Vuex from 'vuex';
import router from '../router';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {},
    errors: '',
    regErr: '',
  },
  mutations: {
    updateUser(state, user) {
      state.user = user;
    },
    updateErrors(state, errors) {
      state.errors = errors;
    },
    updateRegErrors(state, errors) {
      state.regErr = errors;
    },
  },
  actions: {
    async logIn(ctx, [login, password]) {
      const formData = new FormData();
      formData.append('login', login);
      formData.append('password', password);
      try {
        const res = await fetch('http://manao:8888/log-in', {
          method: 'POST',
          body: formData,
        });
        const data = await res.json();
        data.status
          ? (ctx.commit('updateUser', data),
            ctx.commit('updateErrors', ''),
            router.push('/account'),
            (document.cookie = `id=${data.id}`))
          : ctx.commit('updateErrors', data);
        console.log(data);
      } catch (e) {
        console.error(e);
      }
    },
    async register(ctx, [login, pass, confirm, email, name]) {
      const formData = new FormData();
      formData.append('login', login);
      formData.append('password', pass);
      formData.append('confirm', confirm);
      formData.append('email', email);
      formData.append('name', name);
      try {
        const res = await fetch('http://manao:8888/register', {
          method: 'POST',
          body: formData,
        });
        const data = await res.json();
        data.status
          ? (ctx.commit('updateUser', data),
            ctx.commit('updateRegErrors', ''),
            router.push('/account'))
          : ctx.commit('updateRegErrors', data);
        console.log(data);
      } catch (e) {
        console.error(e);
      }
    },
    async logined(ctx) {
      const matches = document.cookie.match(
        new RegExp(
          '(?:^|; )' +
            'id'.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
            '=([^;]*)'
        )
      );
      const formData = new FormData();
      formData.append('matches', matches[1]);
      const res = await fetch('http://manao:8888/logined', {
        method: 'POST',
        body: formData,
      });
      const data = await res.json();
      console.log(data);
      if (data.hasOwnProperty('email')) {
        ctx.commit('updateUser', data), router.push('/account');
      }
    },
    logOut(ctx) {
      ctx.commit('updateUser', {}), router.push('/log-in');
      document.cookie = `id=null`;
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getErrors(state) {
      return state.errors;
    },
    getRegErrors(state) {
      return state.regErr;
    },
  },
});
